#!/usr/bin/python -tt
import hmac
import hashlib
import base64

key = "4875s6O+dw7rxn60GVLxVTyoEKyYIfzvn+hKzD8Cxm/Q0UOMl7EICVZpgXC5cThEiFMkxZcm/NH3fTkqP2FnZJEqIKbK7ptpHsDGaOazMWkoQZByS8hcNeWGoAe5LBh7q4VdwUijQVGy+ugKoHzLPoWxXGSODDhVYlaFNJ4Nj44="
data = "2013-01-25T05:55:40.115Z"
digestmod = hashlib.sha256

dig = hmac.new(key, data, digestmod).digest()
sign = base64.b64encode(dig).decode()
print sign
