#!/usr/bin/env python

import csv
from httplib2 import Http
import json


with open('jobs.csv', 'rb') as jobsRead, open('jobsCost.csv', 'wb') as jobsWrite:
    spamreader = csv.reader(jobsRead, delimiter=',', quotechar=' ')
    spamwriter = csv.writer(jobsWrite, delimiter=',',quotechar=' ', quoting=csv.QUOTE_MINIMAL)
    h = Http()
    for row in spamreader:
        NAME=row[1].replace('"','')
        COSTENDPOINT="https://app.cloudability.com/api/1/reporting/cost/run?auth_token=DCbpRwpVGU2irCa3zSVS&dimensions=usage_type&metrics=invoiced_cost&sort_by=invoiced_cost&order=desc&start_date=2014-01-01&end_date=2015-08-26&filters=tag1%253D%2540" + NAME + "%2Cvendor_account_name%253D%2540Matthew%2520Lowden%2520(MTC%2520dev)"
        reply, content = h.request(COSTENDPOINT, "GET")
        parsed_json  = json.loads(content)
        __type  = ''
        __cost  = '$0';
        __hours = 0;
        if parsed_json['results']:
          __cost = parsed_json['meta']['aggregates'][0]['value']
          print "cost:" + __cost
        

        HOURENDPOINT="https://app.cloudability.com/api/1/reporting/cost/run?auth_token=DCbpRwpVGU2irCa3zSVS&dimensions=usage_type&metrics=usage_hours&start_date=2014-01-01&end_date=2015-08-26&filters=tag1%253D%2540" + NAME + "%2Cvendor_account_name%253D%2540Matthew%2520Lowden%2520(MTC%2520dev)"
        reply, content = h.request(HOURENDPOINT, "GET")
        parsed_json  = json.loads(content)
        if parsed_json['results']:
          __type  = parsed_json['results'][0]['usage_type']
          __hours = parsed_json['results'][0]['usage_hours']
          print "type:" + __type + ", hours:" + str(__hours)
        
        row.append('"' + __cost       + '"')
        row.append('"' + str(__hours) + '"')
        row.append('"' + __type       + '"')
        spamwriter.writerow(row)
