#!/usr/bin/python
import datetime
##import pytz
import time

# Get UTC timestamp
timestamp = time.time()
print timestamp

# Create Python UTC datetime objects that are 'timezone aware'
utc = datetime.datetime.utcfromtimestamp(timestamp);
##utc = pytz.utc.localize(utc)

# Create a couple of timezone objects
##auckland = pytz.timezone('Pacific/Auckland')
##usa_east = pytz.timezone('US/Eastern')

# Format string
format = "%a %e %b %Y %r %Z"

# Print UTC time
print utc.strftime(format)

# Print time in Auckland, New Zealand
##tmp = utc.astimezone(auckland)
##print tmp.strftime(format)

# Print time in Eastern USA
##tmp = utc.astimezone(usa_east)
##print tmp.strftime(format)

