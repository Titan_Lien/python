#! /usr/bin/env python
from Tkinter import *
import tkMessageBox
from datetime import *
import time

timestamp = datetime.utcnow()
slotth = 0
setup_second = 1

def Calculate(setup_second):
	"""Calculate which slot by second"""
	totalsec = ((timestamp.hour *60) + timestamp.minute ) *60 + timestamp.second
	slotth = totalsec / int(setup_second)
	return slotth

class GUIFramework(Frame):
    """This is the GUI"""
	
    def __init__(self,master=None):
        """Initialize yourself"""
        
        """Initialise the base class"""
        Frame.__init__(self,master)
        
        """Set the Window Title"""
        self.master.title("Type Slot Width here")

        """Display the main window"
        with a little bit of padding"""
        self.grid(padx=10,pady=10)
        self.CreateWidgets()
       
    def CreateWidgets(self):
        """Create all the widgets that we need"""
                
        """Create the Text"""
        self.lbText = Label(self, text="Enter Seconds:")
        self.lbText.grid(row=0, column=0)
        
        """Create the Entry, set it to be a bit wider"""
        self.enText = Entry(self)
        self.enText.grid(row=0, column=1, columnspan=3)
        
        """Create the Button, set the text and the 
        command that will be called when the button is clicked"""
        self.btnDisplay = Button(self, text="Get Slot ID", command=self.Display)
        self.btnDisplay.grid(row=0, column=4)
#self.timeout_add(1000, self.Display)
		
    def Display(self):
        """Called when btnDisplay is clicked, displays the contents of self.enText"""
        setup_second = self.enText.get()
        tkMessageBox.showinfo("NOW", "Slot Width is %s sec. \nUTC: %s\n[%d]th slot" % (setup_second, timestamp, Calculate(setup_second)))

if __name__ == "__main__":
	guiFrame = GUIFramework()
	guiFrame.mainloop()

